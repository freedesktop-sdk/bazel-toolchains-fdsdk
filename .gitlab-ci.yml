variables:
  # Store everything under the /builds directory. This is a separate Docker
  # volume. Note that GitLab CI will only cache stuff inside the build
  # directory.
  XDG_CACHE_HOME: "/bst_cache"
  BAZEL_CACHE: "/cache"
  BST: "bst --colors"

stages:
  - build-1
  - build-2
  - test
  - post-test


.docker: &docker
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

push_builder_image:
  <<: *docker
  stage: build-1
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/test-builder
  script:
    - cd docker
    - ( docker pull $IMAGE:latest || true )
    - |
      docker build -f builder.dockerfile \
      --cache-from $IMAGE:latest \
      --tag $IMAGE:$CI_COMMIT_SHA \
      --tag $IMAGE:latest \
      .
    - docker push $IMAGE:$CI_COMMIT_SHA
    - docker push $IMAGE:latest

push_test_image:
  <<: *docker
  stage: build-2
  dependencies:
    - push_builder_image
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/test-image
    BUILDER: $CI_REGISTRY_IMAGE/test-builder
  script:
    - cd docker
    - docker pull $BUILDER:latest
    - ( docker pull $IMAGE:latest || true )
    - |
      docker build -f dockerfile \
      --cache-from $BUILDER:latest \
      --cache-from $IMAGE:latest \
      --build-arg BUILDER=$BUILDER \
      --tag $IMAGE:$CI_COMMIT_SHA \
      .
    - docker push $IMAGE:$CI_COMMIT_SHA

.test_template: &test
  tags:
    - docker-priv-x86_64
  image: $CI_REGISTRY_IMAGE/test-image:$CI_COMMIT_SHA
  stage: test
  dependencies:
    - push_test_image
  before_script:
    - export PATH=~/.local/bin:${PATH}

     # Configure Buildstream
    - mkdir -p ~/.config
    - |
      cat > ~/.config/buildstream.conf << EOF
      # Get a lot of output in case of errors
      logging:
        error-lines: 80

      artifacts:
      - url: https://push.public.aws.celduin.co.uk
        client-cert: ${GITLAB_CAS_PUSH_CERT}
        client-key: ${GITLAB_CAS_PUSH_KEY}
        push: true
      EOF
    - "export PATH=/root/bin:/root/.local/bin/:$PATH"
    - "export USER=root"
    - "export HOME=/root/"
    - cd tests
    - |
      cat > .bazelrc << EOF
      build:fdsdk --crosstool_top=@fdsdk//:fdsdk
      build:fdsdk --cpu=${ARCH}
      EOF
    - cat .bazelrc
    - |
      cat > WORKSPACE << EOF
      load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
      git_repository(
        name = "rules_buildstream",
        branch = "master",
        remote = "https://gitlab.com/celduin/buildstream-bazel/rules_buildstream.git",
      )

      load("@rules_buildstream//bst:defs.bzl", "bst_element")

      local_repository(
        name = "fdsdk_toolchain_repo",
        path = "..",
      )

      bst_element(
        name = "bazel-toolchain",
        element = "${ELEMENT}.bst",
        timeout = 14400,
        quiet = False,
        project_dir = "${BAZEL_CACHE}/external/fdsdk_toolchain_repo",
      )

      load("@fdsdk_toolchain_repo//toolchain:gen_build_defs.bzl", "gen_build_defs")

      gen_build_defs(
        name = "fdsdk",
        arch = "${ARCH}",
        archive = "@bazel-toolchain//bazel-toolchain",
        parent_repo = "@fdsdk_toolchain_repo",
        build_template_file = "@fdsdk_toolchain_repo//toolchain:BUILD.in",
        wrapper_template_file = "@fdsdk_toolchain_repo//toolchain:wrapper.in",
      )

      register_toolchains("@fdsdk//:cc-toolchain")
      EOF
    - cat WORKSPACE
  script:
    - bazel --output_base=${BAZEL_CACHE} build --disk_cache=${BAZEL_CACHE} --config=fdsdk //main:hello-world

test_toolchain_x86-64:
  variables:
    ELEMENT: "toolchain-x86_64-deploy"
    ARCH: "x86_64"
  <<: *test

test_toolchain_aarch64:
  variables:
    ELEMENT: "toolchain-aarch64-deploy"
    ARCH: "aarch64"
  <<: *test

push_latest_image:
  stage: post-test
  <<: *docker
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/test-image
  script:
    - docker pull $IMAGE:$CI_COMMIT_SHA
    - docker tag $IMAGE:$CI_COMMIT_SHA $IMAGE:latest
    - docker push $IMAGE:latest
  only:
    - master
